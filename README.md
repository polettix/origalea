## origalea

This program eases the creation of origami dice with custom faces. You just
provide the faces, as PNG images, and the script will (mostly) do the rest.

### Install

For the moment, you need to put `origalea` in a directory in `PATH` and
`origalea.tp` in the same directory.

It will need the following tools:

- [teepee][] to expand the template
- [cairosvg][] if you want to produce a pdf

### Run

The program is `origalea`:

    $ origalea --pdf -o file1.pdf a.png b.png c.png > die.pdf

Useful place to get icons: [game-icons][]

# COPYRIGHT & LICENSE

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.
>
>  Dedicated to the loving memory of my mother.

[game-icons]: https://game-icons.net/
[cairosvg]: https://cairosvg.org/
[teepee]: https://github.polettix.it/teepee/
